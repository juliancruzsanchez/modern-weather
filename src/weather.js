import * as Capactior from '@capacitor/core'
const { Geolocation } = Capactior.Plugins

export function weather() {
  return new Promise(async (resolve, rej) => {
    let pos = await Geolocation.getCurrentPosition()
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        // Typical action to be performed when the document is ready:
        var forecastReq = new XMLHttpRequest();
        forecastReq.onreadystatechange = function () {
          if (this.readyState == 4 && this.status == 200) {
            resolve(JSON.parse(forecastReq.responseText))
          }
        }
        forecastReq.open("GET", JSON.parse(xhttp.responseText).properties.forecast, true);
        forecastReq.send();
      }
    };
    xhttp.open("GET", `https://api.weather.gov/points/${pos.coords.latitude},${pos.coords.longitude}`, true);
    xhttp.send();
  })

}
