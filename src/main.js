import App from "@/App.vue";
import Vue from "vue";

Vue.config.productionTip = false;

window.Vue = new Vue({
  render: h => h(App)
}).$mount("#app");
